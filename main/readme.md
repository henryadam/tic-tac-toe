I AM JaCK`s AnGry PAraKEet.

I DO NOT PLAY NICE...I GO FIRST

       THE RULES

            I AM X

            I GO FIRST

            YOU LOSE

            I WIN DRAWS

            I AM JaCK`s AnGry PAraKEet


 Thanks to http://forum.codecall.net/topic/50892-tic-tac-toe-in-python/ for the inspiration.
 Thanks to http://bash-shell.net/blog/2013/jan/04/python-tic-tac-toe-ai/ for the class based approach and interesting
 design aspects, an accurate reflection of basic game theory, there are tomes upon tomes of information on the concept of tic-tac-toe in game theory
 and zero-sum games and their strategies, I found this approach easiest to understand and implement in a Django view.

 Thanks to http://www.me.utexas.edu/~jensen/ORMM/supplements/units/game_theory/game_thry.pdf for a cool read on game theory and tic tac toe

 start server from
    /main$ python manage.py runserver

 dont forget to check the requirements.txt